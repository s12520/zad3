package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import domain.CalculatorParameters;
import javax.servlet.*;

public final class orderForm_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\r\n");
      out.write("<html>\r\n");
      out.write("<head>\r\n");
      out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\r\n");
      out.write("<title>dane szczegolowe</title>\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      domain.CalculatorParameters parameters = null;
      synchronized (session) {
        parameters = (domain.CalculatorParameters) _jspx_page_context.getAttribute("parameters", PageContext.SESSION_SCOPE);
        if (parameters == null){
          parameters = new domain.CalculatorParameters();
          _jspx_page_context.setAttribute("parameters", parameters, PageContext.SESSION_SCOPE);
        }
      }
      out.write('\r');
      out.write('\n');
      domain.CalculatorApplication calculator = null;
      synchronized (session) {
        calculator = (domain.CalculatorApplication) _jspx_page_context.getAttribute("calculator", PageContext.SESSION_SCOPE);
        if (calculator == null){
          calculator = new domain.CalculatorApplication();
          _jspx_page_context.setAttribute("calculator", calculator, PageContext.SESSION_SCOPE);
        }
      }
      out.write("\r\n");
      out.write("\r\n");
      out.write("Wybrales umowe: ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.CalculatorParameters)_jspx_page_context.findAttribute("parameters")).getType())));
      out.write(" <br/>\r\n");
      out.write("Wybrales rok: ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.CalculatorParameters)_jspx_page_context.findAttribute("parameters")).getYear())));
      out.write("</br>\r\n");
      out.write("Wybrales kwote: ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.CalculatorParameters)_jspx_page_context.findAttribute("parameters")).getSalary())));
      out.write("</br>\r\n");
      out.write("Wybrales typ kwoty: ");
      out.write(org.apache.jasper.runtime.JspRuntimeLibrary.toString((((domain.CalculatorParameters)_jspx_page_context.findAttribute("parameters")).getNetOrGross())));
      out.write("</br></br>\r\n");
      out.write("<form action=\"orderTableGenerator.jsp\">\r\n");
      out.write("<label>1. Jaki jest Twoj koszt przychodu:</label></br>\r\n");
      out.write("<label>50%<input type=\"radio\" value=\"50\" name=\"incomeCost\" id=\"incomeCost\" checked=\"checked\"/></label></br>\r\n");
      out.write("<label>20%<input type=\"radio\" value=\"20\" name=\"incomeCost\" id=\"incomeCost\"/></label></br></br>\r\n");
      out.write("\r\n");
      out.write("<label>2. Czy odprowadzasz skladki:</label></br>\r\n");
      out.write("<label>a) skladka rentowa:</label></br>\r\n");
      out.write("<label>tak<input type=\"radio\" value=\"1\" name=\"rentalDue\" id=\"rentalDue\"/ checked=\"checked\"></label></br>\r\n");
      out.write("<label>nie<input type=\"radio\" value=\"0\" name=\"rentalDue\" id=\"rentalDue\"/></label></br>\r\n");
      out.write("<label>b) skladka emerytalna:</label></br>\r\n");
      out.write("<label>tak<input type=\"radio\" value=\"1\" name=\"retirementDue\" id=\"retirementDue\" checked=\"checked\"/></label></br>\r\n");
      out.write("<label>nie<input type=\"radio\" value=\"0\" name=\"retirementDue\" id=\"retirementDue\"/></label></br>\r\n");
      out.write("<label>c) skladka chorobowa:</label></br>\r\n");
      out.write("<label>tak<input type=\"radio\" value=\"1\" name=\"sicknessInsurance\" id=\"sicknessInsurance\" checked=\"checked\"/></label></br>\r\n");
      out.write("<label>nie<input type=\"radio\" value=\"0\" name=\"sicknessInsurance\" id=\"sicknessInsurance\"/></label></br>\r\n");
      out.write("<label>d) skladka zdrowotna:</label></br>\r\n");
      out.write("<label>tak<input type=\"radio\" value=\"1\" name=\"healthInsurance\" id=\"healthInsurance\" checked=\"checked\"/></label></br>\r\n");
      out.write("<label>nie<input type=\"radio\" value=\"0\" name=\"healthInsurance\" id=\"healthInsurance\"/></label></br>\r\n");
      out.write("\r\n");
      out.write("<input type = \"submit\" value=\"show result\"/>\r\n");
      out.write("</form>\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
