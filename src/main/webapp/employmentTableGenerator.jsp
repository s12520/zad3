<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding ="ISO-8859-1"%>
<%@ page import = "domain.*" %>
<%@ page import = "managers.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Table</title>
</head>
<body>


<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>
<jsp:useBean id="calculatorService" class="managers.Calculator" scope="application"/>

<jsp:setProperty name="parameters" property="type" param ="type"/>
<jsp:setProperty name="parameters" property="year" param ="year"/>
<jsp:setProperty name="parameters" property="salary" param ="salary"/>
<jsp:setProperty name="parameters" property="netOrGross" param ="netOrGross"/>
<jsp:setProperty name="parameters" property="incomeCost" param ="incomeCost"/>

<% String s = "gross";
String p = parameters.getNetOrGross(); 
double salary = parameters.getSalary();
double sicknessInsurance = 0;
double retirementDue = 0;
double rentalDue = 0;
double healthInsurance = 0;
double root = 0;
double deposit = 0;
%>

<%if (s.equals(p)){ %>
<%parameters.setSalaryBrutto(salary);
sicknessInsurance = parameters.getSalaryBrutto()*(2.45/100);
retirementDue = parameters.getSalaryBrutto()*(9.76/100);
rentalDue = parameters.getSalaryBrutto()*(1.5/100);
healthInsurance = parameters.getSalaryBrutto()*(9.0/100);
root = parameters.getSalaryBrutto()-sicknessInsurance-retirementDue-rentalDue-healthInsurance;
deposit = (parameters.getSalaryBrutto()-root)*0.22;
parameters.setSalaryNetto(root-deposit);%>
<%} else { %>
<%
parameters.setSalaryNetto(salary);
parameters.setSalaryBrutto(parameters.getSalaryNetto()*1.39);
sicknessInsurance = parameters.getSalaryBrutto()*(2.45/100);
retirementDue = parameters.getSalaryBrutto()*(9.76/100);
rentalDue = parameters.getSalaryBrutto()*(1.5/100);
healthInsurance = parameters.getSalaryBrutto()*(9.0/100);
root = parameters.getSalaryBrutto()-sicknessInsurance-retirementDue-rentalDue-healthInsurance;
deposit = (parameters.getSalaryBrutto()-root)*0.22;

%>

<%} %>

<table border='1'>
<tr><td rowspan="2">Miesiac nr</td><td rowspan="2">Brutto</td><td colspan ="4">Ubezpieczenie</td><td rowspan="2">Podstawa opodatkowania</td><td rowspan="2">Zaliczka na PIT</td><td rowspan="2">Netto</td></tr>
<td>emerytalne</td><td>rentowe</td><td>chorobowe</td><td>zdrowotne</td></tr>
<%for(int i = 1; i < 13; i++ ){ %>
<tr><td><%=i%></td>
<td><jsp:getProperty name="parameters" property="salaryBrutto"/></td>
<td><%= Math.round(retirementDue)%></td>
<td><%= Math.round(rentalDue)%></td>
<td><%= Math.round(sicknessInsurance)%></td>
<td><%= Math.round(healthInsurance)%></td>
<td><%= Math.round(root)%></td>
<td><%= Math.round(deposit)%></td>
<td><jsp:getProperty name="parameters" property="salaryNetto"/></td>
</tr>
<%} %>

<tr><td>suma</td>
<td><%= parameters.getSalaryBrutto()*12 %>
<td><%= Math.round(retirementDue)*12%></td>
<td><%= Math.round(rentalDue)*12%></td>
<td><%= Math.round(sicknessInsurance)*12%></td>
<td><%= Math.round(healthInsurance)*12%></td>
<td><%= Math.round(root)*12%></td>
<td><%= Math.round(deposit)*12%></td>
<td><%= parameters.getSalaryNetto()*12 %>
</tr>
</table>

</body>
</html>