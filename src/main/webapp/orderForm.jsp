<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "domain.CalculatorParameters" %>
<%@ page import = "javax.servlet.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>dane szczegolowe</title>
</head>
<body>

<jsp:useBean id="parameters" class="domain.CalculatorParameters" scope="session"/>
<jsp:useBean id="calculator" class="domain.CalculatorApplication" scope="session"/>

Wybrales umowe: <jsp:getProperty name="parameters" property="type"/> <br/>
Wybrales rok: <jsp:getProperty name="parameters" property="year"/></br>
Wybrales kwote: <jsp:getProperty name="parameters" property="salary"/></br>
Wybrales typ kwoty: <jsp:getProperty name="parameters" property="netOrGross"/></br></br>
<form action="orderTableGenerator.jsp">
<label>1. Jaki jest Twoj koszt przychodu:</label></br>
<label>50%<input type="radio" value="50" name="incomeCost" id="incomeCost" checked="checked"/></label></br>
<label>20%<input type="radio" value="20" name="incomeCost" id="incomeCost"/></label></br></br>

<label>2. Czy odprowadzasz skladki:</label></br>
<label>a) skladka rentowa:</label></br>
<label>tak<input type="radio" value="1" name="rentalDue" id="rentalDue"/ checked="checked"></label></br>
<label>nie<input type="radio" value="0" name="rentalDue" id="rentalDue"/></label></br>
<label>b) skladka emerytalna:</label></br>
<label>tak<input type="radio" value="1" name="retirementDue" id="retirementDue" checked="checked"/></label></br>
<label>nie<input type="radio" value="0" name="retirementDue" id="retirementDue"/></label></br>
<label>c) skladka chorobowa:</label></br>
<label>tak<input type="radio" value="1" name="sicknessInsurance" id="sicknessInsurance" checked="checked"/></label></br>
<label>nie<input type="radio" value="0" name="sicknessInsurance" id="sicknessInsurance"/></label></br>
<label>d) skladka zdrowotna:</label></br>
<label>tak<input type="radio" value="1" name="healthInsurance" id="healthInsurance" checked="checked"/></label></br>
<label>nie<input type="radio" value="0" name="healthInsurance" id="healthInsurance"/></label></br>

<input type = "submit" value="show result"/>
</form>

</body>
</html>