package domain;

public class CalculatorParameters {

	private int rentalDue;
	private int retirementDue;
	private int sicknessInsurance;
	private int healthInsurance;
	private float incomeCost;
	private double incomeAmount;
	private String type;
	private int year;
	private double salaryNetto;
	private double salaryBrutto;
	private double salary;
	private String netOrGross;

	
	public int getSkladkaRentowa() {
		return rentalDue;
	}

	public void setSkladkaRentowa(int rentalDue) {
		this.rentalDue = rentalDue;
	}

	public int getSkladkamerytalnaSkladka() {
		return retirementDue;
	}

	public void setSkladkamerytalnaSkladka(int retirementDue) {
		this.retirementDue = retirementDue;
	}

	public int getSkladkaChorobowa() {
		return sicknessInsurance;
	}

	public void setSkladkaChorobowa(int sicknessInsurance) {
		this.sicknessInsurance = sicknessInsurance;
	}

	public int getSkladkaZdrowotna() {
		return healthInsurance;
	}

	public void setSkladkaZdrowotna(int healthInsurance) {
		this.healthInsurance = healthInsurance;
	}

	public float getIncomeCost() {
		return incomeCost;
	}

	public void setIncomeCost(float incomeCost) {
		this.incomeCost = incomeCost;
	}

	public double getIncomeAmount() {
		return incomeAmount;
	}

	public void setIncomeAmount(double incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public String getNetOrGross() {
		return netOrGross;
	}

	public void setNetOrGross(String netOrGross) {
		this.netOrGross = netOrGross;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getSalaryNetto() {
		return salaryNetto;
	}

	public void setSalaryNetto(double salaryNetto) {
		this.salaryNetto = salaryNetto;
	}

	public double getSalaryBrutto() {
		return salaryBrutto;
	}

	public void setSalaryBrutto(double salaryBrutto) {
		this.salaryBrutto = salaryBrutto;
	}
	
	
	
}

