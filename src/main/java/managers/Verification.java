package managers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;




//@WebServlet("/verify.jsp")
public class Verification extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		if (request.getParameter("type").equals("employment")){
			response.sendRedirect("employmentSetter.jsp");
		}else if (request.getParameter("type").equals("performance")){
			response.sendRedirect("performanceForm.jsp");
		}else if (request.getParameter("type").equals("order")){
			response.sendRedirect("orderForm.jsp");
		} else{
			response.sendRedirect("/");
		}
		
}
}
